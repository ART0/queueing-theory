using DataStructures, Distributions

begin
    #definitions of some data types and symbols

    Float = Float64
    Inft = Union{Int, Float}
    ∞ = Inf
end

"""
A struct containing all the relevant details of the customers in the system.

All the relevant attributes like entry time, service time, exit time, etc. are stored as
fields in this struct. Treating each customer as an object with the relevant fields makes
analysis of the queueing system easier.
"""
mutable struct Customer
    n::Int  #the customer number

    nᵢ::Int   #the number of people in the system just before the customer enters
    nₒ::Int   #the number of people in the system just after the customer exits

    tᵢ::Float  #the time at which the customer enters the system
    tₘ::Float  #the time at which the customer moves to the server
    tₒ::Float  #the time at which the customer leaves the system

    τ::Float   #the time taken for the customer to arrive
    σ::Float   #the time taken for the customer to get served

    τᵣ::Float  #the total time spent waiting in the queue
    τₛ::Float  #the total time spent in the system

    Customer(n, nᵢ = 0, nₒ = 0, tᵢ = 0.0, tₘ = 0.0, tₒ = 0.0, τᵣ = 0.0, τₛ = 0.0) =
    new(n, tᵢ, tₘ, tₒ, τᵣ, τₛ)
end

"""
The fields in this struct are the parameters that uniquely determine the behaviour of the queueing system.
These attributes do not change with time, which is why this struct is immutable.

The fields are named according to [Kendall's notation](https://en.wikipedia.org/wiki/Kendall%27s_notation).
"""
struct QueueingModel
    A::Function  #the distribution of inter-arrival times
    S::Function  #the distribution of service times

    c::Inft  #the number of servers
    κ::Inft  #the capacity of the queue
    N::Inft  #the size of the calling population

    select!::Function  #the discipline of the queue
    # - popfirst! for FCFS
    # - pop! for LCFS
    # - popat!(buffer, rand(1:length(buffer))) for random service

    QueueingModel(A, S; c = 1, κ = ∞, N = ∞, select! = popfirst!) =
    new(A, S, c, κ, N, select!)
end

begin
    #Some functions defined for ease-of-use

    Memoryless(λ::Float)::Function = () -> rand(Exponential(λ))
    Deterministic(λ::Float)::Function = () -> λ
end

"""
This struct is the dynamic part of the whole system.

Apart from the static queue parameters, it contains the
`buffer`, `server` and `schedule`, which allow it to handle customers
and schedule tasks dynamically.
"""
mutable struct QueueingSystem
    QP::QueueingModel  #the fixed parameters of the queue

    inbound::Vector{Customer}  #the list of incoming customers
    buffer::Vector{Customer}  #the queue where the customers wait
    server::PriorityQueue{Customer, Float}  #the service area

    n::Int  #the number of customers who have entered the system so far
    schedule::PriorityQueue{Expr, Float}  #the main list of tasks

    t::Float  #the current time
    T::Float  #the time for which the system will run

    customer_log::Vector{Customer}  #a log of all the customers served
    time_log::SortedDict{Float, Expr}  #a log of all the events in the system

    QueueingSystem(QP, T::Float = 100.0) =
    new(QP, Customer[], Customer[], PriorityQueue{Customer, Float}(), 0,
    PriorityQueue{Expr, Float}(), 0.0, T, Customer[], SortedDict{Float, Expr}())
end

"""
Executes the lowest-priority task in the system's schedule, and logs it in the `time_log`.
"""
function execute_task!(Q::QueueingSystem)
    (task, t) = dequeue_pair!(Q.schedule)  #finding the task with the lowest priority

    Q.t = t  #updating the server time
    eval(task) #running the task

    Q.time_log[t] = task  #logging the task
end

"""
Summons the next customer, according to the distribution of inter-arrival times.
"""
function fetch_customer!(Q::QueueingSystem)
    #ensuring that the calling population and the time limit are not exceeded
    if Q.n < Q.QP.N && Q.t < Q.T
        τ = Q.QP.A()  #the time after which the customer will arrive

        customer = Customer(Q.n + 1)  # creating a new customer
        customer.τ = τ

        push!(Q.inbound, customer)  #scheduling the arrival of the new customer

        Q.schedule[:(add_customer!($Q))] = Q.t + τ  #add a customer after time τ
    end
end

"""
Adds a customer to the queue, if the capacity is not exceeded.
"""
function add_customer!(Q::QueueingSystem)
    customer = popfirst!(Q.inbound)  #fetching the new inbound customer

    #enforcing the queue capacity
    if length(Q.buffer) < Q.QP.κ
        Q.n += 1  #update the customer number counter

        customer.tᵢ = Q.t  #recording the time at which the customer has entered
        customer.nᵢ = length(Q.buffer) + length(Q.server)  #the number of people in the system

        println("$(Q.t): Customer $(customer.n) has joined the queue.")

        push!(Q.buffer, customer)  #add the customer to the queue
    end

    fetch_customer!(Q)  #fetch the next customer
    serve_customer!(Q)  #attempt to move customers from the queue to the server
end

"""
Attempts to move the customer from the queue to the service area.
This is limited by the number of available servers.
"""
function serve_customer!(Q::QueueingSystem)
    #checking if there are any free slots in the server
    if length(Q.server) < Q.QP.c && !isempty(Q.buffer)
        customer = Q.QP.select!(Q.buffer)  #picking a customer from the queue
        customer.tₘ = Q.t  #recording the time at which the customer has moved to the server
        customer.τᵣ += Q.t - customer.tᵢ  #updating the time spent waiting in the queue

        σ = Q.QP.S()  #computing service time
        customer.σ = σ

        println("$(Q.t): Customer $(customer.n) is being served.")

        Q.server[customer] = Q.t + σ  #moving the customer to the server
        Q.schedule[:(remove_customer!($Q))] = Q.t + σ  #removing the customer after service
    end
end

"""
Removes a customer with the earliest exit time from the server.
The customer data is stored in the `customer_log` for further analysis.
"""
function remove_customer!(Q::QueueingSystem)
    customer = dequeue!(Q.server)  #picking the customer scheduled to leave the earliest

    customer.tₒ = Q.t  #recording the time at which the customer has exited the server
    customer.τₛ += Q.t - customer.tₘ + customer.τᵣ #updating the time spent in the server
    customer.nₒ = length(Q.buffer) + length(Q.server)  #the number of people in the system

    println("$(Q.t): Customer $(customer.n) has exited the system.")

    push!(Q.customer_log, customer)  #adding the customer to the log

    serve_customer!(Q)  #serving the next customer
end

"""
Gives a quick visual summary of the system.
Customers in the queue are represented by `*`s,
Customers in the server are represented by `#`s,
and a total count of the number of people who have exited the system is displayed.
"""
function display_system(Q::QueueingSystem)
    println("* "^length(Q.buffer) * "   |   " * "# "^length(Q.server) *
    "           || $(length(Q.customer_log))")
end

"""
The main function where the simulation of the queue takes place.
This is the only function that needs to be explicitly called on the `QueueingSystem`.

The `verbose` keyword can be used to display a summary of the system statistics.
"""
function simulate!(Q::QueueingSystem; verbose = true)
    println("The simulation has begun...\n\n")

    fetch_customer!(Q)  #begin by fetching the first customer

    while !isempty(Q.schedule)
        execute_task!(Q)
        display_system(Q)
    end

    println("\n\nThe simulation has ended.")

    if verbose
        println("\n\nSummary of the queueing system statistics:")
        println("============================================")
        collect(compute_statistics(Q)) .|> ((param, val)::Pair{Symbol, Float} -> println("$param: $val"))
        println("============================================")
    end
end

"""
Computes the relevant statistics of the queueing system from the simulation data.

- Average inter-arrival time
- Average waiting time in the queue
- Average time spent in the system
- Average number of customers in the system
- Average number of customers in the queue
- The busy period of the server
"""
function compute_statistics(Q::QueueingSystem)::Dict{Symbol, Float}
    customers = Q.customer_log
    average = (field::Symbol -> mean(getfield.(customers, field)))

    #average inter-arrival time
    Ā = average(:τ)
    #average waiting time in the queue
    W̄ = average(:τᵣ)
    #average time spent in the system
    S̄ = average(:τₛ)
    #average number of customers in the system
    N̄ = (Q.n * S̄) / Q.T  #computed using Little's Law
    #average number of people in the queue
    Q̄ = (Q.n * W̄) / Q.T  #computed using Little's Law
    #the busy period of the server
    pᵦ = (customers .|> (c::Customer -> (iszero(c.nᵢ) ? -c.tᵢ : 0) + (iszero(c.nₒ) ? c.tₒ : 0)) |> sum)/Q.T

    return Dict{Symbol, Float}(zip((:Ā, :W̄, :S̄, :N̄, :Q̄, :pᵦ), (Ā, W̄, S̄, N̄, Q̄, pᵦ)))
end


#A demonstration

queue = QueueingSystem(QueueingModel(Memoryless(10.0), Memoryless(1.0); c = 1), 1000.0)
simulate!(queue)
