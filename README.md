# Simulation of a queue

This is a simulation of a general `A/S/c/K/N` ([notation](https://en.wikipedia.org/wiki/Kendall%27s_notation)) queueing system, with either `FIFO`, `FILO` or `random` queue discipline. The simulation is entirely event-driven, meaning that the system is updated only when a new event (such as the arrival or departure of a customer) occurs. This is different from a continuous time simulation, in which the system is updated at every time step.
